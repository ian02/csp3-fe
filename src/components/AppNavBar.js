import React, { useContext } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';



const AppNavBar = () => {

	const {user} = useContext(UserContext)
	return (
		<Navbar className="color-nav" variant="dark" expand="lg">
					<img
				        src="./frostbean.png"
				        width="40"
				        height="40"
				        className="d-inline-block align-top"
				        alt="Logo"
	     			/>
		           <Link className="navbar-brand" to="/">Frostbean Cream and Coffee Studio</Link>
		           <Navbar.Toggle aria-controls="basic-navbar-nav" />
		           <Navbar.Collapse id="basic-navbar-nav">
		               <Nav className="me-auto">
		                   <Link className="nav-link" to="/products">
		                       {user.isAdmin === true ?
		                               <span>Admin Dashboard</span>
		                           :
		                               <span>Products</span>
		                       }  
		                   </Link>
		               </Nav>
		               <Nav className="mr-auto">
		                   {user.id !== null ?
		                           user.isAdmin === true ?
		                                   <Link className="nav-link" to="/logout">
		                                       Log Out
		                                   </Link>
		                               :
		                                   <React.Fragment>
		                                       <Link className="nav-link" to="/cart">
		                                           Cart
		                                       </Link>
		                                       <Link className="nav-link" to="/orders">
		                                           Orders
		                                       </Link>
		                                       <Link className="nav-link" to="/logout">
		                                           Log Out
		                                       </Link>
		                                   </React.Fragment>
		                       :
		                           <React.Fragment>
		                               <Link className="nav-link" to={{pathname: '/login', state: { from: 'navbar'}}}>
		                                   Log In
		                               </Link>
		                               <Link className="nav-link" to="/register">
		                                   Register
		                               </Link>
		                           </React.Fragment>
		                   }              
		               </Nav>
		           </Navbar.Collapse>
		       </Navbar>
		   );

}

export default AppNavBar;