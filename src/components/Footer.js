import React from "react";
import { FaFacebookF } from "react-icons/fa";
import { FaInstagramSquare} from "react-icons/fa";
import { FaTwitter } from "react-icons/fa";
import { FaMapMarkerAlt } from "react-icons/fa";
import { FaRegCalendarCheck } from "react-icons/fa";

const Footer = () => <footer className="page-footer font-small blue pt-4">
    <div className="container-fluid text-center text-md-left">
        <div className="row">
            <div className="col-md-6 mt-md-0 mt-3">
                <h5 className="text-uppercase">FROSTBEAN CREAM & COFFEE STUDIO</h5>
                <p><FaMapMarkerAlt /> Capitol Drive, Balanga City, Bataan</p>
                <p><FaRegCalendarCheck/> Mon-Sat | 11 am - 8pm </p>
            </div>

            <hr className="clearfix w-100 d-md-none pb-0"/>

            <div className="col-md-3 mb-md-0 mb-3">
                <h5 className="text-uppercase">Useful Links</h5>
                <ul className="list-unstyled">
                    <li><a href="#!">Privacy Policy</a></li>
                    <li><a href="#!">Terms of Use</a></li>
                    <li><a href="#!">Site Map</a></li>
                </ul>
            </div>

            <div className="col-md-3 mb-md-0 mb-3">
                <h5 className="text-uppercase">FOLLOW US:</h5>
                <ul className="list-unstyled">
                    <li><a href="#!"> <FaFacebookF /> </a></li>
                    <li><a href="#!"> <FaInstagramSquare /> </a></li>
                    <li><a href="#!"> <FaTwitter /> </a></li>
                </ul>
            </div>
        </div>
    </div>

    <div className="footer-copyright text-center py-3">© 2022 Copyright : 
        <a href='#!'> frostbeanph.com</a>
    </div>

</footer>

export default Footer;